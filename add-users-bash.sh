#!/bin/bash

if [ $(id -u) -eq 0 ]; then
    echo "This script is being run as root"
else
    echo "*****************************************"
    echo "*         WARNING - WARNING             *"
    echo "* This script is not being run as root  *"
    echo "*        You will need root rights      *"
    echo "*****************************************"

    exit 1
fi


# prompt for number of users

read -p "Enter number of userers to add to system: " num_users

# prompt for username and password

read -p "What is the users prefix before number?: " username_prefix
read -p "what is the default password of all users?: " default_password

# encrypt password for use in adding the user
pass=$(perl -e 'print crypt($ARGV[0], "password")' $default_password)



# prompt for shared group for all users

read -p "What is the name of the shared group name for all new users?: " shared_group_name

#adding the new group

groupadd $shared_group_name

#copy file to /etc/skel 
FILE=/etc/skel/welcome.txt
wget https://gitlab.com/fristdk/itek-bash-scripts/raw/main/welcome_its.txt -O $FILE




for ((i=1; i<=num_users; i++))
do

    #setting username plus the number
    username="$username_prefix$i"
    #echo $username
    useradd "$username" --shell /bin/bash -m -p "$pass" -G "$shared_group_name",sudo

done

# install scapy
apt update && apt upgrade -y


